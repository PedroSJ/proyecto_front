function formatearImporte(importe,decimales){

  importe += ''; // por si pasan un numero en vez de un string
  importe = parseFloat(importe.replace(/[^0-9\.\-]/g, '')); // elimino cualquier cosa que no sea numero o punto

  // si no es un numero o es igual a cero retorno el mismo cero
  if (isNaN(importe) || importe === 0)
      return parseFloat(0).toFixed(decimales);

  // si es mayor o menor que cero retorno el valor formateado como numero
  importe = '' + importe.toFixed(decimales);

  var importe_parts = importe.split('.'),
      regexp = /(\d+)(\d{3})/;

  while (regexp.test(importe_parts[0]))
      importe_parts[0] = importe_parts[0].replace(regexp, '$1' + '.' + '$2');

  return importe_parts.join(',');
}

function formatearFecha(fecha) {
  var fechacompleta = new Date(fecha);
  var dia = fechacompleta.getDate();
  if (dia < 10) {
    dia = "0" + dia;
  }
  var mes = fechacompleta.getMonth() + 1;
  if (mes < 10) {
    mes = "0" + mes;
  }
  var anio = fechacompleta.getFullYear();
  return(dia + "-" + mes + "-" + anio);
}

function formatearFechaFiltro(fecha) {
  return fecha.substring(8,10) + "-" + fecha.substring(5,7) + "-" + fecha.substring(0,4)
}

function validarNIF(nif) {
    var numero, letra, letracorrecta;
    var expresion_regular_nif = /^[XYZ]?\d{1,8}[A-Z]$/;

    if(expresion_regular_nif.test(nif) === true){
        numero = nif.substr(0,nif.length-1);
        numero = numero.replace('X', 0);
        numero = numero.replace('Y', 1);
        numero = numero.replace('Z', 2);
        letra = nif.substr(nif.length-1, 1);
        numero = numero % 23;
        letracorrecta = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letracorrecta = letracorrecta.substring(numero, numero+1);
        if (letracorrecta != letra) {
            //alert('NIF erroneo, la letracorrecta del NIF no se corresponde');
            return false;
        }else{
            //alert('NIF correcto');
            return true;
        }
    }else{
        //alert('NIF erroneo, formato no válido');
        return false;
    }
}

function validarNombre(nombre) {
    if (nombre != "" && nombre != undefined) {
      return true;
    } else {
      return false;
    }
}

function validarApellido(apellido) {
    if (apellido != "" && apellido != undefined) {
      return true;
    } else {
      return false;
    }
}

function validarPassword(password) {
    if (password == undefined) {
      return false
    } else {
      if (password.length < 4 || password.length > 10) {
        return false;
      } else {
        return true;
      }
    }
}

function validarEmail(email) {
    var expresion_regular_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

    if(expresion_regular_email.test(email) === true){
      return true;
    } else {
      return false;
    }
}

function validarTipoMovimiento(tipo) {
    if (tipo != "" && tipo != undefined) {
      return true;
    } else {
      return false;
    }
}

function validarImporte(importe) {
    if (importe == undefined) {
      return false
    } else {
      importe = importe.replace(",",".");
      var expresion_regular_importe = /^\d*(\.\d{1})?\d{0,1}$/;

      if(expresion_regular_importe.test(importe) === true && importe <= 10000 && importe > 0 && importe != ""){
        return true;
      } else {
        return false;
      }
    }
}

function validarImportesFiltro (importedesde, importehasta) {
  importedesde = parseFloat(importedesde.replace(",","."));
  importehasta = parseFloat(importehasta.replace(",","."));
  if (!isNaN(importedesde) && !isNaN(importehasta)) {
    if (importedesde > importehasta) {
      return false
    } else {
      return true
    }
  } else {
    return true
  }
}

function validarFecha(fecha) {
    var hoy = Date.parse(new Date())
    fecha = Date.parse(fecha)
    if (fecha > hoy) {
      return false
    } else {
        return true;
    }
}

function validarFechasFiltro (fechadesde, fechahasta) {
  if (fechadesde != "" && fechahasta != "") {
    if (fechadesde > fechahasta) {
      return false
    } else {
      return true
    }
  } else {
    return true
  }
}

function validarCodigo(codigo) {
    if (codigo != "" && codigo != undefined) {
      return true;
    } else {
      return false;
    }
}

function noAutenticado(objeto) {
  objeto.dispatchEvent(
    new CustomEvent(
      "noautenticado",
      {
        "detail" : "401"
      }
    )
  );
}

function limpiarOlvidoClave(objeto) {
  objeto.nif = "";
  objeto.nifvalido = true;
}

function limpiarNuevaClaveReactivacion(objeto) {
  objeto.codigo = "";
  objeto.password = "";
  objeto.password2 = "";
  objeto.codigovalido = true;
  objeto.passwordvalida = true;
  objeto.password2valida = true;
}

function limpiarNuevoUsuario(objeto) {
  objeto.nif = "";
  objeto.nombre = "";
  objeto.apellido1 = "";
  objeto.apellido2 = "";
  objeto.email = "";
  objeto.password = "";
  objeto.password2 = "";
  objeto.nifvalido = true;
  objeto.nombrevalido = true;
  objeto.apellido1valido = true;
  objeto.apellido2valido = true;
  objeto.emailvalido = true;
  objeto.passwordvalida = true;
  objeto.password2valida = true;
}

function limpiarNuevoMovimiento(objeto) {
  objeto.tipo = "";
  objeto.importe = "";
  objeto.detalle = "";
  objeto.tipovalido = true;
  objeto.importevalido = true;
}

function limpiarDivisas(objeto) {
  objeto.ud = "";
  objeto.divisaorigen = "EUR";
  objeto.divisadestino = "USD";
}

function cerrarVentanas(objeto) {
  objeto.$.listadomovimientos.mostrar = false;
  objeto.$.nuevomovimiento.mostrar = false;
  objeto.$.datosmovimientos.mostrar = false;
  objeto.$.modificacionusuario.mostrar = false;
  objeto.$.cuentas.mostrar = false;
  objeto.$.oficinas.mostrar = false;
  objeto.$.divisas.mostrar = false;
}

function muestraLogin(objeto) {
  objeto.mostrar = true;
  objeto.$.nif.focus();
  objeto.nif = "";
  objeto.password = "";
}
